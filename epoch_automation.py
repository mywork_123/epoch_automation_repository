import pandas as pd
import time,os
import datetime
import argparse

#Requirements
    #pandas 1.5.3

'''
    to read a log file and convert the numeric based UTC time to readable time format.
'''

class epoch_Automation():
    def __init__(self,filename):
        self.file=filename
        self.outcome=[]
    def split_FileContent(self):
        with open(self.file,'r') as f:
            data=f.read()
            f.close()
        return data.splitlines()
    def convert_utcToReadableFormat(self):
        for line in self.split_FileContent():
            utc_date=line.split(':')[0]
            dt=datetime.datetime.fromtimestamp(float(utc_date))
            result_line=time.strftime('%a %W %b %H:%M:%S.{} UTC %Y'.format(dt.microsecond),time.gmtime(float(utc_date)))+':'+':'.join(line.split(':')[1:])
            self.outcome.append(result_line)
    def write_File(self,file):
        f=open(os.path.splitext(filename)[0]+'_result.txt','a')
        for line in self.outcome:
            f.write(line+'\n')
        f.close()
        
if __name__=='__main__':
    parser = argparse.ArgumentParser(description='Pass a log file')
    parser.add_argument('filename', nargs='?')
    args = parser.parse_args()
    filename=args.filename
    print(filename)
    obj=epoch_Automation(filename)
    obj.convert_utcToReadableFormat()
    obj.write_File(filename)
    
    
    
#python <savedfilename.py> <logsfilepath.txt>